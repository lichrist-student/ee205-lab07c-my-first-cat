//////////////////////////////////////////////////////////////////////////////
///////         University of Hawaii, College of Engineering
/////// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///////
/////// Usage: To get used to namespaces and create my first object
///////    
///////
///////
/////// Result:
///////   Prints hello cat in c++
/////// 
///////
/////// @file hello2.cpp
/////// @version 1.0
///////
/////// @author Christian Li <lichrist@hawaii.edu>
/////// @date   27_Feb_2022
///////////////////////////////////////////////////////////////////////////////////
//
//Namespaces
#include <iostream>

int main() {
   std::cout << "Hello world" << std::endl;
   return 0;
}
