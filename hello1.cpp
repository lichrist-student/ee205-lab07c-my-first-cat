//////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
/////
///// Usage: To get used to namespaces and create my first object
/////    
/////
/////
///// Result:
/////   Prints hello cat in c++
///// 
/////
///// @file hello1.cpp
///// @version 1.0
/////
///// @author Christian Li <lichrist@hawaii.edu>
///// @date   27_Feb_2022
/////////////////////////////////////////////////////////////////////////////////
//

//Namespaces
#include <iostream>

using namespace std;

//Prints hello world 
int main() {
   cout << "Hello World" << endl;

   return 0;
}
